import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import KeyPad from './components/NumericKeyboard.vue'
import Assignment from './components/Assignment.vue'

Vue.config.productionTip = false

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

Vue.component('kalkulator-keyboard', KeyPad)
Vue.component('assignment', Assignment)


new Vue({
  render: h => h(App),
}).$mount('#app')
